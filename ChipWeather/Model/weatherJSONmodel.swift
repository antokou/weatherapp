//
//  Cities.swift
//  ChipWeather
//
//  Created by Antonios Koumpenakis on 02/12/2018.
//  Copyright © 2018 Antonios Koumpenakis. All rights reserved.
//


import Foundation

struct Welcome: Codable {
    let cnt: Int?
    let list: [List]?
}

struct List: Codable {
    let coord: Coord?
    let sys: Sys?
    let weather: [Weather]?
    let main: Main?
    let wind: Wind?
    let clouds: Clouds?
    let dt, id: Int?
    let name: String?
}

struct Clouds: Codable {
    let all: Int?
}

struct Coord: Codable {
    let lon, lat: Double?
}

struct Main: Codable {
    let temp, pressure: Double?
    let humidity: Int?
    let tempMin, tempMax, seaLevel, grndLevel: Double?
    
    enum CodingKeys: String, CodingKey {
        case temp, pressure, humidity
        case tempMin = "temp_min"
        case tempMax = "temp_max"
        case seaLevel = "sea_level"
        case grndLevel = "grnd_level"
    }
}

struct Sys: Codable {
    let type, id: Int?
    let message: Double?
    let country: String?
    let sunrise, sunset: Int?
}

struct Weather: Codable {
    let id: Int?
    let main, description, icon: String?
}

struct Wind: Codable {
    let speed, deg: Double?
}
