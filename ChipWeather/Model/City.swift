//
//  City.swift
//  ChipWeather
//
//  Created by Antonios Koumpenakis on 02/12/2018.
//  Copyright © 2018 Antonios Koumpenakis. All rights reserved.
//

// The City struct containts the details of the JSON that we are interested to display
// in our app.

struct City {
    
     var temp: Int
     var maxTemp: Int
     var minTemp: Int
     var weatherDescription: String
     var humidity: String
     var cityName: String
     var country: String
     var  wind: String
    
    
    init(temp:Int, maxTemp:Int, minTemp:Int, weatherDescription:String, humidity:String, cityName:String, country:String, wind:String){
        self.temp = temp
        self.maxTemp = maxTemp
        self.minTemp = minTemp
        self.weatherDescription = weatherDescription
        self.humidity = humidity
        self.cityName = cityName
        self.country = country
        self.wind = wind
    }
    
    
}
