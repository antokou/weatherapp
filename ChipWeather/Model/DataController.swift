//
//  DataController.swift
//  ChipWeather
//
//  Created by Antonios Koumpenakis on 03/12/2018.
//  Copyright © 2018 Antonios Koumpenakis. All rights reserved.
//

import Foundation
import Alamofire

class DataController {
  static let instance = DataController()
  
     // URL used for calls to the OpenWeatherMap API
   let url = "http://api.openweathermap.org/data/2.5/group?id=2643743,6269531,2859887,3648559,2673722,8131587,6154028,5397765,2759794&units=metric&APPID=28800c42d8d22fb0b45133297a7a02ab"
    
     // Method for calls to the OpenWeatherMap API
    func retrieveWeatherInfo(url: String,viewController:ViewController, completionHandler: @escaping (_ data: [City]) -> ()){
        //Create an mpty array of City structs; it will be passed to the completion handler
        var allCitiesArray = [City]()
        
        Alamofire.request(url, method: .get).responseJSON { response in
            //if we get data back
            if let data = response.data {
                //parse the json file
                self.parseJSON(data: data, completionHandler: { (cities) in
                    // Pass what comes back from parseJSON to the allCitiesArray
                    allCitiesArray = cities
                })
            }
            /// handle errors ~~~ (room for improvement: we could check every single error response)
            if response.error != nil {
                //if there is an error show an alert to the user
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Alert", message: "There was an error retrieving the data. Please try later.", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        viewController.viewDidLoad()
                    }))
                    viewController.present(alert, animated: true, completion: nil)
                }
            }
            completionHandler(allCitiesArray)
        }
        
    }
    //////// Parsing the JSON with Decodable //////////
    func parseJSON(data:Data, completionHandler: @escaping (_ data: [City]) -> ()) {
        //create an empty array of City s
        var cityArray = [City]()
        
        do{
            //decode JSON
            let decodedJSON = try JSONDecoder().decode(Welcome.self,from: data)
            let cities = decodedJSON.list
            if cities != nil {
                for city in cities! {
                    //for every city in the JSON create a City struct and append it to the cityArray created earlier
                    cityArray.append(City(temp: Int((city.main?.temp)!), //get current temperature
                        maxTemp: Int((city.main?.tempMax)!), // highest temperature
                        minTemp: Int((city.main?.tempMin)!), //lowest temperature
                        weatherDescription: (city.weather?[0].description)!,//weather description
                        humidity: "\(String((city.main?.humidity)!))" + "% humidity",//humidity
                        cityName: city.name!, //city name
                        country: (city.sys?.country)!, //country name
                        wind: String((city.wind?.speed)!) )) //wind speed
                    
                }
                completionHandler(cityArray)
            }
        } catch let error {
             print("JSON parsing error: \(error.localizedDescription)")
        }
    }
    

}
