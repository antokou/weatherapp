//
//  ViewController.swift
//  ChipWeather
//
//  Created by Antonios Koumpenakis on 02/12/2018.
//  Copyright © 2018 Antonios Koumpenakis. All rights reserved.
//

import UIKit
import Reachability
import Alamofire

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let reachability = Reachability()!
    var cityArray = [City]()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Set up tableView delegate and datasource
        tableView.delegate = self
        tableView.dataSource = self
       
        //set up the navbar title and Refresh button
        self.navigationItem.title = "Cities"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Refresh", style: .plain, target: self, action: #selector(refresh))
    }
    
    @objc func refresh(){
        // Make another API call to retrieve latest data
        DataController.instance.retrieveWeatherInfo(url: DataController.instance.url, viewController: self) { (data) in
            self.cityArray=[]
            self.cityArray = data
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Add observer for Reachability notifications
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: reachability)
    }
    
    //MARK: - Check the internet connection
    /***************************************************************/
   
    @objc func reachabilityChanged(note: Notification) {
        
        let reachability = note.object as! Reachability
        //Monitors changes in the connection
        switch reachability.connection {
        case .wifi:
            // Enable the Refresh button on the navbar
            self.navigationItem.rightBarButtonItem?.isEnabled=true
            // Make the API call to get the weather data
            DataController.instance.retrieveWeatherInfo(url: DataController.instance.url, viewController: self) { (data) in
                //empty the cityArray(to avoid duplicate data)
                self.cityArray=[]
                //fill the cityArray with the array com
                self.cityArray = data
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        case .cellular:
            // Enable the Refresh button on the navbar
            self.navigationItem.rightBarButtonItem?.isEnabled=true
            //make the API call to get the weather data
            DataController.instance.retrieveWeatherInfo(url: DataController.instance.url, viewController: self) { (data) in
                //empty the cityArray(to avoid duplicate data)
                self.cityArray=[]
                self.cityArray = data
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
        }
        //If there is not a connection it informs the user and disables the refresh button
        case .none:
            // Disable the Refresh button on the navbar
            self.navigationItem.rightBarButtonItem?.isEnabled=false
           // Show an alert informing the user that they don't have connection
            let alert = UIAlertController(title: "Internet connection unavailable.", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }
        
    }
    
    
    //MARK: - Cities Table View
    /***************************************************************/
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cityArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cityName") as? CityCellTableViewCell else { return UITableViewCell() }
        let city = self.cityArray[indexPath.row]
        // Set up our cell by giving it the city and country name
        cell.setupCell(cityLabel: city.cityName, countryLabel: city.country)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // For the tapped city we get the City struct
        let citySelected = cityArray[indexPath.row]
        let DetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        // and we pass it to the initializer of the DetailViewController
        DetailViewController.initCity(forCity: citySelected)
        self.navigationController?.pushViewController(DetailViewController, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

