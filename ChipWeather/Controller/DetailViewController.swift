//
//  DetailViewController.swift
//  ChipWeather
//
//  Created by Antonios Koumpenakis on 02/12/2018.
//  Copyright © 2018 Antonios Koumpenakis. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var currentTempLabel: UILabel!
    @IBOutlet weak var weatherDescriptionLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    
    var city:City?
    func initCity(forCity: City) {
        city = forCity
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Set up all the labels in the view with the values from the city
        currentTempLabel.text = "\(city?.temp ?? 0)ºC"
        weatherDescriptionLabel.text = city?.weatherDescription ?? ""
        minTempLabel.text = "lowest temperature: \(city?.minTemp ?? 0)ºC"
        maxTempLabel.text = "highest temperature: \(city?.maxTemp ?? 0)ºC"
        humidityLabel.text = city?.humidity ?? ""
        windSpeedLabel.text = "\(city?.wind ?? "")mph"
        self.navigationItem.title = "\(city?.cityName ?? "")"

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
       

}
