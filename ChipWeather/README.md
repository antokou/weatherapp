#  Test for Chip Financial Ltd.



## What is ChipWeather
It is a weather app using OpenMapWeather.Org API data. 
The first view controller(ViewController) loads a list of cities. When a cell is tapped it presents the city's weather info on the second view controller(DetailsViewController).

## App logic
ViewController: 
* Upon launch we check the internet connection; if connected, it makes the API call. If at any point the connection goes off the Refresh button is disabled. When the connection comes back on, the Refresh button is enabled and a call to retrieve the latest weather data is performed.
* If the API call is succesful it passes the received JSON for decoding.
* We use Decodable to Parse the JSON. For every city we parse, we create a City struct that contains all the properties that we want to show in the DetailViewController. The City then added in a [City]
* Upon parsing completion we reload the tableView. In the ViewController we only show city and country name.
* When a cell(city) is tapped it passes the City struct for that city via an initializer.


DetailViewController: 
* Before the view appears it receives the City struct, gets all the property values and assign them to          
                                   the respective labels.
* Details shows is the current temperature, weather description, lowest and highest temperature, humidity and wind speed.
                                

## External libraries used
I used Reachability ( https://github.com/ashleymills/Reachability.swift ) to monitor internet connectivity.
And Alamofire ( https://github.com/Alamofire/Alamofire ) to make the API calls.




