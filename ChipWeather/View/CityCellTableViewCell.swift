//
//  CityCellTableViewCell.swift
//  ChipWeather
//
//  Created by Antonios Koumpenakis on 02/12/2018.
//  Copyright © 2018 Antonios Koumpenakis. All rights reserved.
//

import UIKit


class CityCellTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    
    func setupCell(cityLabel:String, countryLabel:String) {
        self.cityLabel.text = cityLabel
        self.countryLabel.text = countryLabel
    }

}
