//
//  ChipWeatherTests.swift
//  ChipWeatherTests
//
//  Created by admin on 02/12/2018.
//  Copyright © 2018 antonios koumpenakis. All rights reserved.
//

import XCTest
import Alamofire

@testable import ChipWeather

class ChipWeatherTests: XCTestCase {

    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        // Setup mock
       
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    //MARK: - Testing the URL
    /***************************************************************/
    

    func testURL() {
        let url = URL(string: "http://api.openweathermap.org/data/2.5/group?id=2643743,6269531,2759794&units=metric&APPID=28800c42d8d22fb0b45133297a7a02ab")
        let promise = expectation(description: "Response: 200")
        
        let task = Alamofire.request(url!, method: .get).responseJSON { (response) in
           if response.error != nil {
                XCTAssert(false, "Error: \(String(describing: response.error?.localizedDescription))")
                return
            }
           else {
               // if the status code is 200 we have the correct url
               if response.response?.statusCode == 200 {
                 promise.fulfill()
               }
               else {
                 XCTAssert(false, "Code: \(String(describing: response.response?.statusCode))")
               }
           }
        }
        task.resume()
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    //MARK: - Testing the JSON parsing
    /***************************************************************/
    
    func testParseJSON() {
        //The JSON.txt file contains the response data for
        // http://api.openweathermap.org/data/2.5/group?id=2643743&units=metric&APPID=28800c42d8d22fb0b45133297a7a02ab
        let bundle = Bundle(for: type(of: self))
        if let path = bundle.path(forResource: "JSON", ofType: "txt") {
            if let data = try? Data.init(contentsOf: URL.init(fileURLWithPath: path)) {

                    DataController.instance.parseJSON(data: data, completionHandler: { (cityArray) in
                        //checks if it parses correctly the city
                        XCTAssertEqual(cityArray[0].cityName, "London")
                        //checks if it parses correctly the country
                        XCTAssertEqual(cityArray[0].country, "GB")
                        //checks if it parses correctly the temperature
                        XCTAssertEqual(cityArray[0].temp, Int(8.2))
                        //checks if it parses correctly the highest temperature
                        XCTAssertEqual(cityArray[0].maxTemp, Int(9))
                        //checks if it parses correctly the minimum temperature
                        XCTAssertEqual(cityArray[0].minTemp, Int(7))
                        //checks if it parses correctly the weather description
                        XCTAssertEqual(cityArray[0].weatherDescription, "broken clouds")
                        //checks if it parses correctly the weather description
                        XCTAssertEqual(cityArray[0].weatherDescription, "broken clouds")
                        //checks if it parses correctly the weather description
                        XCTAssertEqual(cityArray[0].humidity, "66% humidity")
                        //checks if it parses correctly the weather description
                        XCTAssertEqual(cityArray[0].wind, "5.1")
                        
                    })
            }
            else {
                XCTFail()
            }
        }
        else {
            XCTAssert(false, "There is no file in the path")
        }
    }
}



